<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function show(){
        $webname='clickweb';
        $user='Admin';
        $userType = $user == 'Admin' ? true : false;
        return view('admin',['web'=>$webname, 'usertype'=>$userType]);
    }
}
