<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1 align="center">includeWhen</h1>
    @includeWhen($usertype, 'software') <!-- is admin than call -->
    <hr>
    @includeUnless($usertype, 'software') <!-- admin do not than call -->
    @includeUnless(!$usertype, 'software') <!-- is admin than call -->
</body>
</html>