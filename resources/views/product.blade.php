<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product</title>
</head>
<body>
    <h1 align="center">Product</h1>
    <hr>
    @include('software')
    <hr>
    @includeIf('software1') {{-- If check include file are avable --}}
</body>
</html>